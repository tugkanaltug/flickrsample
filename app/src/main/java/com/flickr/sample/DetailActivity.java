package com.flickr.sample;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.flickr.sample.model.Photo;
import com.flickr.sample.ui.component.WrapContentDraweeView;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        if (getIntent().hasExtra("KEY")) {
            Photo photo = getIntent().getParcelableExtra("KEY");
            WrapContentDraweeView image = findViewById(R.id.image);
            image.setup("https://farm" + photo.getFarm() + ".staticflickr.com/" + photo.getServer() + "/" +
                    photo.getId() + "_" + photo.getSecret() + ".jpg");
        }
    }
}
