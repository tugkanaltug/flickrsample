package com.flickr.sample.ui.main;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.flickr.sample.BuildConfig;
import com.flickr.sample.data.FlickrApi;
import com.flickr.sample.data.output.PhotoContainerOutput;
import com.flickr.sample.data.output.PhotoOutput;
import com.flickr.sample.model.Photo;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainViewModel extends ViewModel {

    private int pages, page = 1;
    private MutableLiveData<List<Photo>> mData = new MutableLiveData<>();
    private FlickrApi api;

    public MainViewModel() {
        api = new Retrofit.Builder()
                .baseUrl(BuildConfig.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build()
                .create(FlickrApi.class);
        request();
    }

    MutableLiveData<List<Photo>> getData() {
        return mData;
    }

    void refresh() {
        page = 1;
        request();
    }

    void next() {
        if (page < pages) {
            page++;
            request();
        }
    }

    private void request() {
        Call<PhotoContainerOutput> call = api.getRecentPhotos(String.valueOf(page));
        call.enqueue(new Callback<PhotoContainerOutput>() {
            @Override
            public void onResponse(Call<PhotoContainerOutput> call, Response<PhotoContainerOutput> response) {
                try {
                    if (response.isSuccessful() && response.body() != null) {
                        page = response.body().getPhotos().getPage();
                        pages = response.body().getPhotos().getPages();
                        ArrayList<Photo> list = new ArrayList<>();
                        for (PhotoOutput output : response.body().getPhotos().getPhoto())
                            list.add(new Photo(output.getFarm(), output.getServer(), output.getId(), output.getSecret(), output.getTitle()));
                        mData.setValue(list);
                    } else {
                        onError();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    onError();
                }
            }

            @Override
            public void onFailure(Call<PhotoContainerOutput> call, Throwable t) {
                t.printStackTrace();
                onError();
            }
        });
    }

    private void onError() {
        mData.setValue(null);
    }

}