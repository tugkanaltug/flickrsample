package com.flickr.sample.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.flickr.sample.databinding.ItemMainBinding
import com.flickr.sample.model.Photo
import javax.inject.Inject

class MainAdapter @Inject constructor() : RecyclerView.Adapter<MainViewHolder>() {

    private val items = ArrayList<Photo>()
    var listener: Listener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemMainBinding.inflate(inflater, parent, false)
        return MainViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        holder.bind(items[position], listener)
    }

    override fun getItemCount() = items.size

    fun addAll(items: List<Photo>) {
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    fun clear() {
        this.items.clear()
        notifyDataSetChanged()
    }

    interface Listener {
        fun onItemClick(photo: Photo, imageView: ImageView)
    }

}
