package com.flickr.sample.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.flickr.sample.DetailActivity;
import com.flickr.sample.R;
import com.flickr.sample.databinding.FragmentMainBinding;
import com.flickr.sample.util.HandlerUtil;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.google.android.material.snackbar.Snackbar;

public class MainFragment extends Fragment implements NestedScrollView.OnScrollChangeListener {

    private FragmentMainBinding binding;

    private MainViewModel viewModel;

    private MainAdapter adapter;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (binding == null)
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(getContext());
        layoutManager.setFlexDirection(FlexDirection.ROW);
        layoutManager.setJustifyContent(JustifyContent.FLEX_END);

        adapter = new MainAdapter();
        adapter.setListener((photo, imageView) -> {
            Intent intent = new Intent(getContext(), DetailActivity.class);
            intent.putExtra("KEY", photo);
            startActivity(intent);
        });

        binding.nestedScrollView.setOnScrollChangeListener(this);
        binding.swipeRefreshLayout.setOnRefreshListener(() -> refresh());
        binding.recyclerView.setLayoutManager(layoutManager);
        binding.recyclerView.setAdapter(adapter);

        viewModel = new ViewModelProvider(this).get(MainViewModel.class);
        viewModel.getData().observe(getViewLifecycleOwner(), items -> HandlerUtil.runOnUiThread(() -> {
            binding.swipeRefreshLayout.setRefreshing(false);
            if (items == null) {
                Snackbar.make(binding.getRoot(), R.string.an_error_occurred_please_try_again, Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.ok, view -> refresh()).show();
            } else {
                adapter.addAll(items);
            }
        }));
    }

    @Override
    public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        if (v.getChildAt(v.getChildCount() - 1) != null) {
            if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) && scrollY > oldScrollY) {
                binding.swipeRefreshLayout.setRefreshing(true);
                viewModel.next();
            }
        }
    }

    private void refresh() {
        adapter.clear();
        viewModel.refresh();
    }
}