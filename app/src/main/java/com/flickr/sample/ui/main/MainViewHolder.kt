package com.flickr.sample.ui.main

import androidx.recyclerview.widget.RecyclerView
import com.flickr.sample.databinding.ItemMainBinding
import com.flickr.sample.model.Photo
import com.google.android.flexbox.AlignSelf.STRETCH
import com.google.android.flexbox.FlexboxLayoutManager

class MainViewHolder(private val binding: ItemMainBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(photo: Photo, listener: MainAdapter.Listener?) {
        binding.item = photo
        binding.executePendingBindings()
        binding.image.apply {
            setOnClickListener {
                listener?.onItemClick(photo, this)
            }
            if (layoutParams is FlexboxLayoutManager.LayoutParams) {
                val layoutParams = layoutParams as FlexboxLayoutManager.LayoutParams
                layoutParams.flexGrow = 1.0f
                layoutParams.alignSelf = STRETCH
            }
        }
    }
}