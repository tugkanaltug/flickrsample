package com.flickr.sample.data.output

data class PhotoWrapperOutput(
        val page: Int,
        val pages: Int,
        val total: Int,
        val photo: List<PhotoOutput>
)