package com.flickr.sample.data.output

data class PhotoOutput(
        val farm: String,
        val server: String,
        val id: String,
        val secret: String,
        val title: String
)