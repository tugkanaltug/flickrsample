package com.flickr.sample.data

import com.flickr.sample.BuildConfig
import com.flickr.sample.data.output.PhotoContainerOutput
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface FlickrApi {
    @GET("services/rest/?" +
            "method=flickr.photos.getRecent&" +
            "nojsoncallback=1&" +
            "format=json&" +
            "per_page=20&" +
            "api_key=" + BuildConfig.API_KEY)
    fun getRecentPhotos(@Query("page") page: String?): Call<PhotoContainerOutput?>?
}