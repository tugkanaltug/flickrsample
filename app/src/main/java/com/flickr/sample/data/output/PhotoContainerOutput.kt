package com.flickr.sample.data.output

data class PhotoContainerOutput(
        val photos: PhotoWrapperOutput
)